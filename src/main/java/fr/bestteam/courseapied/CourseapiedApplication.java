package fr.bestteam.courseapied;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseapiedApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourseapiedApplication.class, args);
	}

}
