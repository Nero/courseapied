package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Categorie;


public interface CategorieManager {
	
	Categorie addCategorie(Categorie categorie) throws ManagerException;

	void updateEvenement(Categorie categorie) throws ManagerException;

	List<Categorie> findAll();

	Categorie findById(Long id);

	void deleteById(Long id) throws ManagerException;

}
