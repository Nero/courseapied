package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Categorie;
import fr.bestteam.courseapied.dal.CategorieDAO;

@Service
public class CategorieManagerImpl implements CategorieManager {
	
	
	@Autowired
	CategorieDAO dao;
	
	
	@Override
	public Categorie addCategorie(Categorie categorie) throws ManagerException {
		return dao.save(categorie);
		
	}

	@Override
	public void updateEvenement(Categorie categorie) throws ManagerException {
		dao.save(categorie);
		
	}

	@Override
	public List<Categorie> findAll() {
	
		return (List<Categorie>) dao.findAll();
	}

	@Override
	public Categorie findById(Long id) {
		// TODO Auto-generated method stub
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		dao.deleteById(id);
		
	}

}
