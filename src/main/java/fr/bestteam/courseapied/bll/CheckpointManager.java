package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Checkpoint;


public interface CheckpointManager {
	
	Checkpoint addCheckpoint(Checkpoint chkp) throws ManagerException;

	void updatePerson(Checkpoint chkp) throws ManagerException;

	List<Checkpoint> findAll();

	Checkpoint findById(Long id);

	void deleteById(Long id) throws ManagerException;

}
