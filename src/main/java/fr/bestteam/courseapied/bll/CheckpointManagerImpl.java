package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Checkpoint;
import fr.bestteam.courseapied.dal.CheckpointDAO;

@Service
public class CheckpointManagerImpl implements CheckpointManager{

	@Autowired
	CheckpointDAO dao;
	
	@Override
	public Checkpoint addCheckpoint(Checkpoint chkp) throws ManagerException {
		return dao.save(chkp);
		
	}

	@Override
	public void updatePerson(Checkpoint chkp) throws ManagerException {
		dao.save(chkp);
		
	}

	@Override
	public List<Checkpoint> findAll() {
		return (List<Checkpoint>)dao.findAll();
	}

	@Override
	public Checkpoint findById(Long id) {
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		dao.deleteById(id);
		
	}

}
