package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Club;


public interface ClubManager {
	
	Club addClub(Club club) throws ManagerException;

	void updateClub(Club club) throws ManagerException;

	List<Club> findAll();

	Club findById(Long id);

	void deleteById(Long id) throws ManagerException;
}


