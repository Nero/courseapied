package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Club;
import fr.bestteam.courseapied.dal.ClubDAO;

@Service
public class ClubManagerImpl implements ClubManager {
	
	
	@Autowired
	ClubDAO dao;

	@Override
	public Club addClub(Club club) throws ManagerException {
	return dao.save(club);
		
	}

	@Override
	public void updateClub(Club club) throws ManagerException {
	dao.save(club);
		
	}

	@Override
	public List<Club> findAll() {
		
		return (List<Club>)dao.findAll();
	}

	@Override
	public Club findById(Long id) {
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		dao.deleteById(id);
		
	}

}
