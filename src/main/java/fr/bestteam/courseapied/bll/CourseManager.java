package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Course;


public interface CourseManager {
	

	Course addCourse(Course course) throws ManagerException;

	void updatePerson(Course course) throws ManagerException;

	List<Course> findAll();

	Course findById(Long id);

	void deleteById(Long id) throws ManagerException;

	
}
