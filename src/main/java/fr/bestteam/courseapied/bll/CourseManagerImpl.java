package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Course;
import fr.bestteam.courseapied.dal.CourseDAO;


@Service
public class CourseManagerImpl implements CourseManager {

	
	@Autowired
	CourseDAO dao;
	
	
	@Override
	public Course addCourse(Course course) throws ManagerException {
		return dao.save(course);
		
	}
	

	@Override
	public void updatePerson(Course course) throws ManagerException {
		dao.save(course);
		
	}

	@Override
	public List<Course> findAll() {
		
		return (List<Course>)dao.findAll();
	}

	@Override
	public Course findById(Long id) {
		
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
	dao.deleteById(id);
		
	}

	
}
