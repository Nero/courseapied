package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Dossard;

public interface DossardManager {

	Dossard addDossar(Dossard dossard) throws ManagerException;

	void updatePerson(Dossard dossard) throws ManagerException;

	List<Dossard> findAll();

	Dossard findById(Long id);

	void deleteById(Long id) throws ManagerException;
}
