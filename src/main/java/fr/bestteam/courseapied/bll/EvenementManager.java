package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Evenement;

public interface EvenementManager {
	Evenement addEvenement(Evenement evenement) throws ManagerException;

	void updateEvenement(Evenement evenement) throws ManagerException;

	List<Evenement> findAll();

	Evenement findById(Long id);

	void deleteById(Long id) throws ManagerException;

}
