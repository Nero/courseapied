package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Evenement;
import fr.bestteam.courseapied.dal.EvenementDAO;



@Service
public class EvenementManagerImpl implements EvenementManager {
	
	@Autowired
	EvenementDAO dao;
	
	
	@Override
	public Evenement addEvenement(Evenement evenement) throws ManagerException {
		if (null==evenement.getLabel()) {
			 throw new ManagerException("Fail to create : Null Label!");
		} else {
			return dao.save(evenement);
		}
	}

	@Override
	public void updateEvenement(Evenement evenement) throws ManagerException {
		if (null==evenement.getId()) {
			 throw new ManagerException("Fail to create : Null Label!");
		} else {
			if (null==evenement.getLabel()) {
				 throw new ManagerException("Fail to create : Null Label!");
			} else {
				dao.save(evenement);
			}
		}
	}
	
	@Override
	public List<Evenement> findAll() {
		
		return (List<Evenement>)dao.findAll();
	}

	
	@Override
	public Evenement findById(Long id) {
		
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		if (null == id || id <= 0) {
            throw new ManagerException("Fail to delete : ID is not valid!");
        }else {
        	dao.deleteById(id);
        }
	}
}
