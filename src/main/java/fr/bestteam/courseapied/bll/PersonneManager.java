package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.Personne;

public interface PersonneManager {

	Personne addPerson(Personne personne) throws ManagerException;

	void updatePerson(Personne personne) throws ManagerException;

	List<Personne> findAll();

	Personne findById(Long id);

	void deleteById(Long id) throws ManagerException;
}
