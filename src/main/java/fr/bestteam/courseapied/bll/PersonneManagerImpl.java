package fr.bestteam.courseapied.bll;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.Personne;
import fr.bestteam.courseapied.dal.PersonneDAO;

@Service
public class PersonneManagerImpl implements PersonneManager {

	@Autowired
	PersonneDAO dao;

	@Override
	@Transactional
	public Personne addPerson(Personne personne) throws ManagerException {
		return dao.save(personne);

	}

	@Override
	@Transactional
	public void updatePerson(Personne personne) throws ManagerException{
		dao.save(personne);

	}

	@Override
	public List<Personne> findAll() {

		return (List<Personne>) dao.findAll();
	}

	@Override
	public Personne findById(Long id) {

		return findById(id);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		dao.deleteById(id);

	}

}
