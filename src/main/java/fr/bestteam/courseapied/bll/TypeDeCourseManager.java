package fr.bestteam.courseapied.bll;

import java.util.List;

import fr.bestteam.courseapied.bo.TypeDeCourse;

public interface TypeDeCourseManager {
	
	TypeDeCourse addTypeDeCourse(TypeDeCourse tc) throws ManagerException;

	void updateTypeDeCourse(TypeDeCourse tc) throws ManagerException;

	TypeDeCourse findById(Long id);

	void deleteById(Long id) throws ManagerException;

	List<TypeDeCourse> findAll();

}
