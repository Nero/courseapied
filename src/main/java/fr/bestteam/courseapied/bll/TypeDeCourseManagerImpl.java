package fr.bestteam.courseapied.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.courseapied.bo.TypeDeCourse;
import fr.bestteam.courseapied.dal.TypeDeCourseDAO;


@Service
public class TypeDeCourseManagerImpl implements TypeDeCourseManager{
	
	@Autowired
	TypeDeCourseDAO dao;

	@Override
	public TypeDeCourse addTypeDeCourse(TypeDeCourse tc) throws ManagerException {
		return dao.save(tc);
		
	}

	@Override
	public void updateTypeDeCourse(TypeDeCourse tc) throws ManagerException {
		dao.save(tc);
		
	}

	@Override
	public TypeDeCourse findById(Long id) {
		return findById(id);
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		dao.deleteById(id);
		
	}

	@Override
	public List<TypeDeCourse> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
