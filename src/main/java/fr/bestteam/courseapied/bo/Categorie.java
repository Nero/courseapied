package fr.bestteam.courseapied.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "categorie")
public class Categorie implements Serializable{
	
	/**
	 * Entite categorie
	 */
	
	private static final long serialVersionUID = 1L;
	
	///////////
	//attribut
	///////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	private String label;
	
	////////////
	//Jointures?
	////////////
	
	@ManyToOne
	@JoinColumn (name = "idCourse")
	private Course course;

	///////////////
	//constructeur
	///////////////
	
	public Categorie() {
		super();
	}

	public Categorie(String label, Course course) {
		super();
		this.label = label;
	}

	////////////////
	//getter setter
	////////////////
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "Categorie [id=" + id + ", label=" + label + ", course=" + course + "]";
	}

	///////////
	//toString
	///////////
	
	
	
	

}
