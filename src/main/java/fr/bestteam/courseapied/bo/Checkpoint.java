package fr.bestteam.courseapied.bo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "checkpoint")
public class Checkpoint implements Serializable{
	
	/**
	 * Entity checkpoint
	 */
	private static final long serialVersionUID = 1L; 
	
	///////////////
	//attributs
	///////////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long id;
	
	private String label;
	private LocalDateTime temps;
	
	////////////
	//Jointures
	////////////
	

	@ManyToOne
	@JoinColumn (name = "idCourse")
	private Course course;
	
	
	@ManyToMany
	private Set<Dossard> dossard= new HashSet<>();
	

	///////////////
	//constructeur
	//////////////
	
	public Checkpoint() {
		super();
	}

	public Checkpoint(long id, String label, LocalDateTime temps) {
		super();
		this.id = id;
		this.label = label;
		this.temps = temps;

	}

	////////////////
	//getter setter
	///////////////

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public LocalDateTime getTemps() {
		return temps;
	}

	public void setTemps(LocalDateTime temps) {
		this.temps = temps;
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	///////////////
	//toString
	///////////////

	@Override
	public String toString() {
		return "Checkpoint [id=" + id + ", label=" + label + ", temps=" + temps + ", course=" + course + "]";
	}
	
	

	
	
}
