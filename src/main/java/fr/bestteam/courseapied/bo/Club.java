package fr.bestteam.courseapied.bo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "club")
public class Club {
	/**
	 * Entite Club
	 */
	public static final long serialVersionUID = 1L;
	
	////////////
	//attribut
	///////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long id;
	private String nom;
	private String adresse;
	private String ville;
	private String codePostal;
	private String tel;
	private String mail;
	
	/////////////
	//jointure
	////////////
	
	@OneToMany (fetch = FetchType.EAGER,
			mappedBy = "club",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	private Set<Personne> personnes = new HashSet<>();

	///////////////
	//constructeur
	///////////////
	
	public Club() {
		
	}

	public Club(String nom, String adresse, String ville, String codePostal, String tel, String mail,
			Personne persone) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.ville = ville;
		this.codePostal = codePostal;
		this.tel = tel;
		this.mail = mail;

	}

	////////////////
	//getter setter
	////////////////
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	


	public Set<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(Set<Personne> personnes) {
		this.personnes = personnes;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}


	////////////
	//toString
	////////////
	
	@Override
	public String toString() {
		return "Club [id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", ville=" + ville + ", codePostal="
				+ codePostal + ", tel=" + tel + ", mail=" + mail + "]";
	}
	
	
	
	
	
	
	
}
