package fr.bestteam.courseapied.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "course")
public class Course implements Serializable{
	
	/**
	 * Entity Course
	 */
	private static final long serialVersionUID = 1L;
	
	//////////////
	//attributs
	//////////////
	@Id
	private long id;
	
	@Column(name="label")
	private String label;
	
	@Column(name="lieu")
	private String lieu;

	
	private Integer distance;
	private Integer nombreParticipants;
	private Integer denivele;
	private Date dateDebut;
	private Date dateFin;
	
	////////////
	//joinbture
	////////////
	
//	@ManyToOne
//	@JoinColumn(name = "idEvenement")
	private Evenement evenement;
	
	@ManyToOne
	@JoinColumn(name = "idTypeDeCourse")
	private TypeDeCourse typeDeCourse;
	
	@OneToMany (fetch = FetchType.EAGER,
			mappedBy = "course",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	private Set<Categorie> categorie = new HashSet<>();
	
	@OneToMany (fetch = FetchType.EAGER,
			mappedBy = "course",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	private Set<Dossard> dossard = new HashSet<>();
	
	@OneToMany (fetch = FetchType.EAGER,
			mappedBy = "course",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	private Set<Checkpoint> checkpoint = new HashSet<>();

	private TypeDeCourse type;
	
	///////////////
	//constructeur
	///////////////
	public Course() {
		
	}

	public Course(String label, String lieu, TypeDeCourse type, Integer distance,
			Integer nombreParticipants, Integer denivele,
			Categorie categorie, Date dateDebut, Date dateFin) {
	
		this.label = label;
		this.lieu = lieu;
		this.type = type;
		this.distance = distance;
		this.nombreParticipants = nombreParticipants;
		this.denivele = denivele;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	
	////////////////
	//getter/setter
	////////////////

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public TypeDeCourse getType() {
		return getType();
	}

	public void setType(TypeDeCourse type) {
		this.type = type;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public int getNombreParticipants() {
		return nombreParticipants;
	}

	public void setNombreParticipants(Integer nombreParticipants) {
		this.nombreParticipants = nombreParticipants;
	}

	public int getDenivele() {
		return denivele;
	}

	public void setDenivele(Integer denivele) {
		this.denivele = denivele;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
	
	public Evenement getEvenement() {
		return evenement;
	}

	public void setEvenement(Evenement evenement) {
		this.evenement = evenement;
	}

	public TypeDeCourse getTypeDeCourse() {
		return typeDeCourse;
	}

	public void setTypeDeCourse(TypeDeCourse typeDeCourse) {
		this.typeDeCourse = typeDeCourse;
	}

	public Set<Categorie> getCategorie() {
		return categorie;
	}

	public void setCategorie(Set<Categorie> categorie) {
		this.categorie = categorie;
	}

	public Set<Dossard> getDossard() {
		return dossard;
	}

	public void setDossard(Set<Dossard> dossard) {
		this.dossard = dossard;
	}

	public Set<Checkpoint> getCheckpoint() {
		return checkpoint;
	}

	public void setCheckpoint(Set<Checkpoint> checkpoint) {
		this.checkpoint = checkpoint;
	}

	///////////////
	//toString
	//////////////
	
	@Override
	public String toString() {
		return "Course [id=" + id + ", label=" + label + ", lieu=" + lieu + ", type=" + type + ", distance=" + distance
				+ ", nombreParticipants=" + nombreParticipants + ", denivele=" + denivele + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
	}
	
	
}
