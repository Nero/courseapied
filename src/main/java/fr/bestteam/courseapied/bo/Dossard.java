package fr.bestteam.courseapied.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "dossard")
public class Dossard implements Serializable {
	
	/**
	 * Entite dossard
	 */
	
	public static final long serialVersionUID = 1L;
	
	/////////////
	//attribut
	/////////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long id;
	
	private Integer numeroDossard;
	
	
	///////////
	//jointure?
	///////////
	
	@ManyToMany (fetch = FetchType.EAGER,
			mappedBy = "dossard",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	private Set<Checkpoint> checkpoint = new HashSet<>();
	
	@ManyToOne
	@JoinColumn (name = "idCourse")
	private Course course;
	
	@ManyToOne
	@JoinColumn (name = "idPersonne")
	private Personne personne;
	
	///////////////
	//constructeur
	///////////////
	
	public Dossard() {
		super();
	}


	public Dossard(long id, Integer numeroDossard) {
		super();
		this.id = id;
		this.numeroDossard = numeroDossard;
	}

	////////////////
	//getter setter
	////////////////

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNumeroDossard() {
		return numeroDossard;
	}

	public void setNumeroDossard(Integer numeroDossard) {
		this.numeroDossard = numeroDossard;
	}

	public Course getCourse() {
		return course;
	}


	public void setCourse(Course course) {
		this.course = course;
	}


	public Personne getPersonne() {
		return personne;
	}


	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	///////////
	//toString
	///////////
	
	@Override
	public String toString() {
		return "Dossard [id=" + id + ", numeroDossard=" + numeroDossard + ", course="
				+ course + ", personne=" + personne + "]";
	}
	
	

	
	
	
	

	
	
	
}
