package fr.bestteam.courseapied.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "evenement")
public class Evenement implements Serializable {
	
	/**
	 * Entite Evenement
	 */
	public static final long serialVersionUID = 1L;
	
	///////////
	//attribut
	///////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	private String label;
	private Date dateDebut;
	private Date dateFin;
	private String lieu;
	
	////////////
	//jointures
	////////////
	

	@OneToMany (fetch = FetchType.EAGER,
			mappedBy = "evenement",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
    private Set<Course> course= new HashSet<>();

	/////////////
	//constructor
	/////////////
	
	public Evenement() {		
	}
	
	public Evenement(String label, Date dateDebut, Date dateFin, String lieu) {
		this.label = label;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
	}
	
	public Evenement(String label) {
		this.label = label;
	}

	///////////////
	//getter setter
	///////////////
	
	public Long getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	
	public Set<Course> getCourse() {
		return course;
	}

	public void setCourse(Set<Course> course) {
		this.course = course;
	}
	
	///////////
	//toString
	///////////
	
	@Override
	public String toString() {
		return "Evenement [id=" + id + ", label=" + label + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin
				+ ", lieu=" + lieu + "]";
	}
	

	/////////////
	// M�thodes
	/////////////
	

}
