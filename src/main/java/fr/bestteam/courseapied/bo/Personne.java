package fr.bestteam.courseapied.bo;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.time.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
@Table(name = "personne")
public class Personne implements Serializable {

	private static final long serialVersionUID = 1l;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nom", nullable = false, length = 25)
	private String nom;

	@Column(name = "Prenom", nullable = false, length = 25)
	private String prenom;

	@Column(name = "civilite", nullable = false, length = 15)
	private String civilite;

	@Column(name = "license", nullable = false)
	private String license;

	@Column(name = "certificatMedical", nullable = false)
	private Boolean certificatMedical = false;

	@Column(name = "date")
	private LocalDate dateNaissance;

	@Column(name = "mail", nullable = false)
	private String mail;

	@Column(name = "tel", length = 15)
	private String tel;

	@Column(name = "adresse", nullable = false)
	private String adresse;

	@Column(name = "codePostale", nullable = false)
	private String codePostale;

	@Column(name = "ville", nullable = false)
	private String ville;

	@ManyToOne
	@JoinColumn(name = "idClub")
	private Club club;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "personne", cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
	private Set<Dossard> dossards = new HashSet<>();

	public Personne() {
	}

	
	public Personne(String nom, String prenom, String civilite, String license, Boolean certificatMedical,
			LocalDate dateNaissance, String mail, String tel, String adresse, String codePostale, String ville,
			Club club, Set<Dossard> dossards) {
	
		this.nom = nom;
		this.prenom = prenom;
		this.civilite = civilite;
		this.license = license;
		this.certificatMedical = certificatMedical;
		this.dateNaissance = dateNaissance;
		this.mail = mail;
		this.tel = tel;
		this.adresse = adresse;
		this.codePostale = codePostale;
		this.ville = ville;
		this.club = club;
		this.dossards = dossards;
	}


	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public Set<Dossard> getDossards() {
		return dossards;
	}

	public void setDossards(Set<Dossard> dossards) {
		this.dossards = dossards;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}


	public String getLicense() {
		return license;
	}


	public void setLicense(String license) {
		this.license = license;
	}


	public Boolean getCertificatMedical() {
		return certificatMedical;
	}

	public void setCertificatMedical(Boolean certificatMedical) {
		this.certificatMedical = certificatMedical;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostale() {
		return codePostale;
	}

	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civilite=" + civilite + ", license="
				+ license + ", certificatMedical=" + certificatMedical + ", dateNaissance=" + dateNaissance + ", mail="
				+ mail + ", tel=" + tel + ", adresse=" + adresse + ", codePostale=" + codePostale + ", ville=" + ville
				+ ", club=" + club + ", dossards=" + dossards + "]";
	}

}
