package fr.bestteam.courseapied.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "type")
public class TypeDeCourse implements Serializable {
	
	/**
	 * Entity Type
	 */
	public static final long serialVersionUID =1L;

	////////////
	//attributs
	////////////
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	private String label;
	
	
	////////////
	//Jointures
	////////////
	
	@OneToMany (fetch = FetchType.EAGER, 
			mappedBy = "typeDeCourse",
			cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
	private Set<Course> course = new HashSet<>();
	
	///////////////
	//constructeur
	///////////////
	
	public TypeDeCourse() {
		super();
	}
	
	public TypeDeCourse(String label, Course course) {
		super();
		this.label = label;
	}


	///////////////
	//getter setter
	///////////////
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}

	///////////
	//toString
	///////////
	
	@Override
	public String toString() {
		return "Type [id=" + id + ", label=" + label + "]";
	}
	

	
}
