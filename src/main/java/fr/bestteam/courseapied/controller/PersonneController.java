package fr.bestteam.courseapied.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.bestteam.courseapied.bll.PersonneManagerImpl;
import fr.bestteam.courseapied.bll.TypeDeCourseManager;
import fr.bestteam.courseapied.bo.Categorie;
import fr.bestteam.courseapied.bo.Checkpoint;
import fr.bestteam.courseapied.bo.Club;
import fr.bestteam.courseapied.bo.Course;
import fr.bestteam.courseapied.bo.Dossard;
import fr.bestteam.courseapied.bo.Evenement;
import fr.bestteam.courseapied.bo.Personne;
import fr.bestteam.courseapied.bo.TypeDeCourse;
import fr.bestteam.courseapied.bll.CategorieManager;
import fr.bestteam.courseapied.bll.CheckpointManager;
import fr.bestteam.courseapied.bll.ClubManager;
import fr.bestteam.courseapied.bll.CourseManager;
import fr.bestteam.courseapied.bll.DossardManager;
import fr.bestteam.courseapied.bll.DossardManagerImpl;
import fr.bestteam.courseapied.bll.EvenementManager;
import fr.bestteam.courseapied.bll.EvenementManagerImpl;
import fr.bestteam.courseapied.bll.ManagerException;
import fr.bestteam.courseapied.bll.PersonneManager;

@RestController
@RequestMapping("/courseapied")
@CrossOrigin(origins = "http://localhost:4200")
public class PersonneController {

	@Autowired
	PersonneManager mgrPers;

	@Autowired
	EvenementManager mgrEve;
	
	@Autowired
	DossardManager mgrImDo;
	
	@Autowired
	CategorieManager mgrCat;
	
	@Autowired
	CheckpointManager mgrChk;
	
	@Autowired
	ClubManager mgrClub;
	
	@Autowired
	CourseManager mgrCourse;
	
	@Autowired
	TypeDeCourseManager mgrTypeCourse;
	
	
	/*@RequestMapping("/personne")
	@ResponseBody
	public ResponseEntity<List> getPersonne() {
		return new ResponseEntity(managerIm.findAll(), HttpStatus.OK);
	}*/
	/////////////////////////////
	// Les personnes
	/////////////////////////////
	
	@GetMapping("/personne")
	public List<Personne> findAll() {

		return mgrPers.findAll();
	}

	@GetMapping("/personne/{id}")
	public Personne getOnePersonneById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/personne/" + id);
		return mgrPers.findById(id);
	}

	@PostMapping("/personne")
	public Personne addPerson(@RequestBody Personne personne) throws Exception {
		try {
			return mgrPers.addPerson(personne);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;

	}

	@PostMapping("/personne/{id}")
	public Personne personneById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/personne/" + id);
		return mgrPers.findById(id);
	}

	@PutMapping("/personne")
	public void updatePerson(@RequestBody Personne personne) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/personne");
		try {
			mgrPers.updatePerson(personne);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/personne/{id}")
	public void supprimerPersonne(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/personne");
		try {
			mgrPers.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	
	//////////////////////////
	// Les évènements
	//////////////////////////
	@GetMapping("/evenement")
	public List<Evenement> findAllEv() {

		return mgrEve.findAll();
	}
	
	@GetMapping("/evenement/{id}")
	public Evenement getOneEvenementById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/evenement/" + id);
		return mgrEve.findById(id);
	}

	@PostMapping("/evenement")
	public Evenement addEvenement(@RequestBody Evenement evenement) throws Exception {
		try {
			return mgrEve.addEvenement(evenement);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/evenement/{id}")
	public Evenement evenementById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/evenement/" + id);
		return mgrEve.findById(id);
	}

	@PutMapping("/evenement")
	public void updateEvenement(@RequestBody Evenement evenement) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/evenement");
		try {
			mgrEve.updateEvenement(evenement);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/evenement/{id}")
	public void supprimerEvenement(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/evenement");
		try {
			mgrEve.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	
	//////////////////////////
	// Les dossards
	//////////////////////////

	@GetMapping("/dossard")
	public List<Dossard> findAllDo() {

		return mgrImDo.findAll();
	}
	
	@GetMapping("/dossard/{id}")
	public Dossard getOneDossardById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/dossard/" + id);
		return mgrImDo.findById(id);
	}

	@PostMapping("/dossard")
	public Dossard addDossard(@RequestBody Dossard dossard) throws Exception {
		try {
			return mgrImDo.addDossar(dossard);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/dossard/{id}")
	public Dossard dossardById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/dossard/" + id);
		return mgrImDo.findById(id);
	}

	@PutMapping("/dossard")
	public void updateDossard(@RequestBody Dossard dossard) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/dossard");
		try {
			mgrImDo.updatePerson(dossard);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/dossard/{id}")
	public void supprimerDossard(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/dossard");
		try {
			mgrImDo.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	//////////////////////////
	// Les catégories
	//////////////////////////

	@GetMapping("/categorie")
	public List<Categorie> findAllCat() {

		return mgrCat.findAll();
	}
	
	@GetMapping("/categorie/{id}")
	public Categorie getOneCategorieById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/categorie/" + id);
		return mgrCat.findById(id);
	}

	@PostMapping("/categorie")
	public Categorie addCategorie(@RequestBody Categorie categorie) throws Exception {
		try {
			return mgrCat.addCategorie(categorie);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/categorie/{id}")
	public Categorie ById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/categorie/" + id);
		return mgrCat.findById(id);
	}

	@PutMapping("/categorie")
	public void updateCategorie(@RequestBody Categorie categorie) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/categorie");
		try {
			mgrCat.updateEvenement(categorie);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/categorie/{id}")
	public void supprimerCategorie(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/categorie");
		try {
			mgrCat.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	//////////////////////////
	// Les checkpoints
	//////////////////////////

	@GetMapping("/checkpoint")
	public List<Checkpoint> findAllCheck() {

		return mgrChk.findAll();
	}
	
	@GetMapping("/checkpoint/{id}")
	public Checkpoint getOneCheckById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/checkpoint/" + id);
		return mgrChk.findById(id);
	}

	@PostMapping("/checkpoint")
	public Checkpoint addCheck(@RequestBody Checkpoint checkpoint) throws Exception {
		try {
			return mgrChk.addCheckpoint(checkpoint);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/checkpoint/{id}")
	public Checkpoint checkpointById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/checkpoint/" + id);
		return mgrChk.findById(id);
	}

	@PutMapping("/checkpoint")
	public void updateCheckpoint(@RequestBody Checkpoint checkpoint) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/checkpoint");
		try {
			mgrChk.updatePerson(checkpoint);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/checkpoint/{id}")
	public void supprimerCheckpoint(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/checkpoint");
		try {
			mgrChk.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	//////////////////////////
	// Les clubs
	//////////////////////////

	@GetMapping("/club")
	public List<Club> findAllClub() {

		return mgrClub.findAll();
	}
	
	@GetMapping("/club/{id}")
	public Club getOneClubById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/club/" + id);
		return mgrClub.findById(id);
	}

	@PostMapping("/club")
	public Club addClub(@RequestBody Club club) throws Exception {
		try {
			return mgrClub.addClub(club);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/club/{id}")
	public Club clubById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/club/" + id);
		return mgrClub.findById(id);
	}

	@PutMapping("/club")
	public void updateClub(@RequestBody Club club) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/club");
		try {
			mgrClub.updateClub(club);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/club/{id}")
	public void supprimerClub(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/Club");
		try {
			mgrClub.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	//////////////////////////
	// Les courses
	//////////////////////////

	@GetMapping("/course")
	public List<Course> findAllCourse() {

		return mgrCourse.findAll();
	}
	
	@GetMapping("/course/{id}")
	public Course getOneCourseById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/course/" + id);
		return mgrCourse.findById(id);
	}

	@PostMapping("/course")
	public Course addCourse(@RequestBody Course course) throws Exception {
		try {
			return mgrCourse.addCourse(course);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/course/{id}")
	public Course courseById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/course/" + id);
		return mgrCourse.findById(id);
	}

	@PutMapping("/course")
	public void updateCourse(@RequestBody Course course) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/course");
		try {
			mgrCourse.updatePerson(course);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/course/{id}")
	public void supprimerCourse(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/course");
		try {
			mgrCourse.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	//////////////////////////
	// Les types de courses
	//////////////////////////

	@GetMapping("/typecourse")
	public List<TypeDeCourse> findAllTypeCourse() {

		return mgrTypeCourse.findAll();
	}
	
	@GetMapping("/typedecourse/{id}")
	public TypeDeCourse getOneTypeCourseById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/typedecourse/" + id);
		return mgrTypeCourse.findById(id);
	}

	@PostMapping("/typedecourse")
	public TypeDeCourse addTypeCourse(@RequestBody TypeDeCourse typeDeCourse) throws Exception {
		try {
			return mgrTypeCourse.addTypeDeCourse(typeDeCourse);
			
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}

	@PostMapping("/typedecourse/{id}")
	public TypeDeCourse typeCourseById(@PathVariable Long id) throws Exception {
		System.out.println("REST - GET - http://<DNS>/courseapied/typedecourse/" + id);
		return mgrTypeCourse.findById(id);
	}

	@PutMapping("/typedecourse")
	public void updateTypeCourse(@RequestBody TypeDeCourse typedecourse) {
		System.out.println("REST - PUT - http://<DNS>/courseapied/typedecourse");
		try {
			mgrTypeCourse.updateTypeDeCourse(typedecourse);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	@DeleteMapping("/typedecourse/{id}")
	public void supprimerTypeCourse(@PathVariable Long id) {
		System.out.println("REST - DELETE - http://<DNS>/courseapied/typedecourse");
		try {
			mgrTypeCourse.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
}
