package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Categorie;
/**
 * Interface CRUD <Categorie, Type id>
 * @author Laurent
 */
public interface CategorieDAO extends CrudRepository<Categorie, Long>{

}
