package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Checkpoint;
/**
 * Interface CRUD <Chekpoint, Type id>
 * @author Laurent
 */
public interface CheckpointDAO extends CrudRepository<Checkpoint, Long>{

}
