package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Club;
/**
 * Interface CRUD <Club, Type id>
 * @author Laurent
 */
public interface ClubDAO extends CrudRepository<Club, Long> {

}
