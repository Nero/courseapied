package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Course;
/**
 * Interface CRUD <Course, Type id>
 * @author Laurent
 */
public interface CourseDAO extends CrudRepository <Course, Long>{

}
