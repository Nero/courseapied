package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Dossard;
/**
 * Interface CRUD <Dossard, Type id>
 * @author Laurent
 */
public interface DossardDAO extends CrudRepository<Dossard, Long>{

}
