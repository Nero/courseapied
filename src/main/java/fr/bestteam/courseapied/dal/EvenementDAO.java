package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.courseapied.bo.Evenement;
/**
 * Interface CRUD <Evenement, Type id>
 * @author Laurent
 */
public interface EvenementDAO extends CrudRepository<Evenement, Long> {

}
