package fr.bestteam.courseapied.dal;

import org.springframework.data.repository.CrudRepository;
import fr.bestteam.courseapied.bo.TypeDeCourse;

public interface TypeDeCourseDAO extends CrudRepository<TypeDeCourse, Long>{

}
