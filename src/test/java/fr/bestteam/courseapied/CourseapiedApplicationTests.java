package fr.bestteam.courseapied;


import java.time.LocalDate;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import fr.bestteam.courseapied.bll.ManagerException;
import fr.bestteam.courseapied.bll.PersonneManager;
import fr.bestteam.courseapied.bo.Evenement;
import fr.bestteam.courseapied.bo.Personne;
import fr.bestteam.courseapied.bll.EvenementManager;



@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseapiedApplicationTests {

	@Autowired
	PersonneManager manager;
	
	@Autowired
	EvenementManager mngEv;
	
	@Transactional
	@Test
	public void contextLoads() throws ManagerException {
		
//		List<Personne>personnes=new ArrayList<>();
//		List<Evenement>evenements=new ArrayList<>();
		

		//TEST D'AJOUT DES PERSSONNES
		
		Personne p1 = new Personne(
				"Braulio", "Pita", "Mr", "oui", true, LocalDate.now(),
				"lifa@hotmail.com", "0695800575", "12 rue des roseaux",
				"44000", "La Limoziniere", null, null);
		
		Personne p2 = new Personne("Tomas", "Cruz", "Mr", "oui", true, LocalDate.now(),
				"lifa@hotmail.com", "06958006655", "12 rue des roseaux",
				"44000", "La Limoziniere", null, null);
		
		Personne p3 = new Personne("Braulio", "Pita", "Mr", "oui", true, LocalDate.now(),
				"lifa@hotmail.com", "0695800575", "12 rue des roseaux",
				"44000", "La Limoziniere", null, null);
		
		Personne p4 = new Personne("Jennifer", "Lopez", "Mrs", "oui", true, LocalDate.now(),
				"lifa@hotmail.com", "0695800575", "12 rue des roseaux",
				"44000", "La Limoziniere", null, null);
		
		/*for (Personne personne : manager.findAll()) {
			System.out.println(personne);
		}*/
		
		/*//Personne res = manager.findByNom("Zita Eva");
		//System.out.println(res.getTel());

		/*try {
			manager.addPerson(personne);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}*/

		//manager.addPerson(personne);
		manager.addPerson(p1);
		manager.addPerson(p2);
		manager.addPerson(p3);
		manager.addPerson(p4);

		
		//AJOUT D'EVENEMENT
//		Evenement e1 = new Evenement("SuperCourse", null, null,"Nantes" );
//		Evenement e2 = new Evenement("SuperCourse", null, null,"Nantes" );
//		Evenement e3 = new Evenement("SuperCourse", null, null,"Nantes" );
//		Evenement e4 = new Evenement("SuperCourse", null, null,"Nantes" );
		Evenement e1 = new Evenement("La vie c'est la course");
		Evenement e2 = new Evenement("Les courses du petit Robert");
		Evenement e3 = new Evenement("Speedy Gonzales");
		Evenement e4 = new Evenement("Championnat des petits Suisses");
		
		
//		evenements.add(e1);
//		evenements.add(e2);
//		evenements.add(e3);
//		evenements.add(e4);

		mngEv.addEvenement(e1);
		mngEv.addEvenement(e2);
		mngEv.addEvenement(e3);
		mngEv.addEvenement(e4);
		
		//manager.addPerson(p1);*/
				
		//montrer liste avec stream
//		System.out.println("-------------------------------------");
//		p1.getDossards().stream().forEach(item->System.out.println(item));
//		System.out.println("-------------------------------------");
		
	}
	
}
